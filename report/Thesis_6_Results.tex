\chapter{Results}
\label{chapter:results}

\section{Dhrystone Benchmark Results For RTL Simulation}

To validate IObSoC and measure its performance on its several operating modes, a simple test tool 
using the Dhrystone benchamark~\cite{bib:dhrystone} was developed. This benchmark is also used in 
PicoRV32's repository to measure the \ac{CPU}'s performance. The test tool performed several 
\ac{RTL} simulations using the Icarus Verilog simulator for different configurations of IObSoC and 
the Dhrystone benchmark~\cite{bib:iobsoc_dhry}. The several \ac{RTL} simulations performed correspond 
to different combinations of the following parameters:

\begin{itemize}
	\item \textbf{IObSoC's operating mode};
	\item The usage of PicoRV32's \textbf{\ac{LA} memory interface};
	\item \textbf{IObSoC's cache configuration}, which changed its size;
	\item The \textbf{number of runs} in the Dhrystone benchmark (i.e., the number of loop 
	iterations of the main code. The used values were 100 and 500;
	\item The usage or not of the \textbf{\code{-O3} GCC flag} for code size and time 
	optimization.
\end{itemize}

All simulations have the testbench feeding IObSoC with a 100 MHz clock signal and were executed 
with no bootloader (\code{USE\_BOOT} commented), as this feature does not affect the results and 
would otherwise take a much greater time to complete the simulations and produce a much larger 
\ac{VCD} file. However, all of IObSoC's operating modes (including those with the bootloader 
feature) were successfully tested and held positive results using the Dhrystone benchmark via 
\ac{RTL} simulation, which validates the \ac{SoC}'s operation.

The obtained results are shown in Tables~\ref{table:results_100} and~\ref{table:results_100_o3}, 
which correspond to 100 runs and the Dhrystone source code compiled without and with the \code{-O3} 
GCC flag, respectively. However, note that the \code{-Os} flag (for code size optimization) is used 
on all simulations, as IObSoC's firmware Makefile uses it by default. 

The results of simulations with 500 runs on Dhrystone (with and without the \code{-O3} GCC flag) 
are not presented in this document because their deviation relatively to the results of the 100 
runs simulations were less than 1\% on the measured performance indicators (\ac{CPI}, \ac{DPS}/MHz 
and \ac{DMIPS}/MHz).

On one hand, Tables~\ref{table:results_100} and~\ref{table:results_100_o3} show the number of cycles
and instructions needed to complete the Dhrystone benchmark execution, both of which are measured in 
the source code by using the \code{rdcycle} and \code{rdinstret} RISC-V assembly instructions, respectively.
These are used immediately prior and after the main code loop to obtain the respective initial and final 
values, which are then subtracted to obtain the time duration in clock cycles and the number of executed 
instructions in the main loop. Afterwards, these values are used to calculate:

\begin{itemize}
	\item \textbf{\ac{CPI} rate estimate of the CPU}, which is the average number of clock cycles 
	that takes for a single instruction to be executed;
	
	\item \textbf{\ac{DPS} per MHz}, the number of iterations through the main code loop per second.
	This value is further divided by the CPU's clock frequency in MHz to allow comparison between Dhrystone
	results of other CPUs running at different clock frequencies. It is also rounded down to produce an integer
	result;
	
	\item \textbf{\ac{DMIPS} per MHz}. \ac{DMIPS} is given by Equation \ref{eq:dmips}, where 1757 is the 
	number of \ac{MIPS} obtained when running Dhrystone on the VAX 11/780, a machine with nominal 
	1 \ac{MIPS}~\cite{bib:dhry_vax}. \ac{DMIPS} is a better Dhrystone performance indicator than \ac{MIPS} 
	because when comparing Dhrystone results on machines with different instructions sets (especially when 
	comparing \ac{RISC} and \ac{CISC} machines), the instruction count is not directly comparable. 
	This value is further divided by the CPU's clock frequency in MHz to allow comparison between Dhrystone 
	results of other CPUs running at different clock frequencies;
\end{itemize}

\begin{equation}
\label{eq:dmips}
\text{DMIPS} = \frac{\text{DPS}}{1757}
\end{equation}

Tests using 500 runs were also made (with and without the \code{-O3} GCC flag), but the deviation 
from the results of the 100 runs tests were minimal (less than 1\%) on all measured performance 
indicators (i.e., number of cycles, \ac{CPI}, \ac{DPS}/MHz and \ac{DMIPS}/MHz), which is why the 500 runs 
tests' results are not shown in this document.

\begin{table*}[!htb]
	\renewcommand{\arraystretch}{1.2} % more space between rows
	\caption{Dhrystone benchmark results on RTL simulation with 100 runs and a clock frequency of 100 MHz.}
	\centering
	\small
	\begin{tabular}{l|cc|cccccc}
		\toprule
		IObSoC memory configuration   & No. cycles & No. instructions & CPI & DPS/MHz & DMIPS/MHz\\
		\midrule
		SRAM                   & 267791 & 48719 & 5.496      & 373         & 0.212\\
		DDR (8 KB Cache)       & 268301 & 48719 & 5.507      & 372         & 0.211\\
		DDR (256 B Cache)      & 387639 & 48719 & 7.956      & 257         & 0.146\\
		\midrule
		SRAM + LA              & 197971 & 48719 & 4.063      & 505         & 0.287\\
		DDR (8 KB Cache) + LA  & 198508 & 48719 & 4.074      & 503         & 0.286\\
		DDR (256 B Cache) + LA & 320918 & 48719 & 6.587      & 311         & 0.177\\
		\bottomrule
	\end{tabular}
	\label{table:results_100}
\end{table*}

\begin{table*}[!htb]
	\renewcommand{\arraystretch}{1.2} % more space between rows
	\caption{Dhrystone benchmark results on RTL simulation with 100 runs, a clock frequency of 100 MHz and the \code{-O3} flag on GCC.}
	\centering
	\small
	\begin{tabular}{l|cc|cccccc}
		\toprule
		IObSoC memory configuration   & No. cycles & No. instructions & CPI  & DPS/MHz & DMIPS/MHz\\
		\midrule
		SRAM                   & 226110 & 40724 & 5.552      & 442         & 0.251\\
		DDR (8 KB Cache)       & 226624 & 40724 & 5.564      & 441         & 0.250\\
		DDR (256 B Cache)      & 319373 & 40724 & 7.842      & 313         & 0.178\\
		\midrule
		SRAM + LA              & 166785 & 40724 & 4.095      & 599         & 0.340\\
		DDR (8 KB Cache) + LA  & 167318 & 40724 & 4.108      & 597         & 0.339\\
		DDR (256 B Cache) + LA & 261254 & 40724 & 6.415      & 382         & 0.217\\
		\bottomrule
	\end{tabular}
	\label{table:results_100_o3}
\end{table*}

The number of executed instructions does not vary for simulations of the same configuration 
of IObSoC because the firmware binary file is exactly the same.

\subsubsection{CPU}

As expected, the measured \ac{CPI} is greater than 1, as PicoRV32 is a RISC processor. 
The most ideal memory configuration to measure this result is the internal \ac{SRAM} connected to the 
CPU via its \ac{LA} memory interface, thus minimizing memory timing delays. The obtained \ac{CPI} for 
this configuration is approximately 4.1, which coincides with PicoRV32's reference \ac{CPI} value, 
thus validating IObSoC.

\subsubsection{Cache}

The number of cycles changes between simulations of the same IObSoC memory configuration. 
As expected, the number of cycles is greater when using the \ac{DDR}, because extra cycles exist 
when a cache read miss occurs and it fetches new instructions/data from the DDR. 

% big cache
Performance is heavily influenced by the Cache's configuration. When the Cache is large (in this case, 
8 KB size), the DDR results are very similar to the SRAM's ones, where the deviation is less than 1\% on all 
performance indicators. This happens because the Cache (also featuring \ac{SRAM} memory) does not produce a 
high number of Cache read misses and therefore does not lose much time fetching instructions or data from the 
\ac{DDR} (which is actually an \ac{AXI} memory on the testbench).

% small cache
However, when using a smaller Cache (in this case, 256 B), the results show a decrease in performance. 
Depending on the memory configuration, the number of cycles and \ac{CPI} increase between 41\% and 62\% 
and the \ac{DPS}/MHz and \ac{DMIPS}/MHz decrease between 29\% and 38\%.

\subsubsection{\code{-O3} GCC optimization flag}

When using the \code{-O3} \ac{GCC} flag, code size and time are optimized and thus the number of cycles decreases
between 15\% and 19\% and the number of executed instructions decreases by 16\%. This confirms that Dhrystone's 
firmware was indeed optimized, as it executes less instructions within less time. The \ac{CPI} increases very 
slightly (1\%) on \ac{SRAM} and \ac{DDR} with large (8 KB) cache configurations, while on \ac{DDR} with small
(256 B) cache configurations decreases also very slightly (between 1,4\% and 2,6\%). The \ac{DPS}/MHz and 
\ac{DMIPS}/MHz increase between 18.5\% and 22.8\%, depending on the memory configuration.

\subsubsection{LA memory interface}

The usage of PicoRV32's \ac{LA} memory interface decreases the number of cycles and the \ac{CPI} by 26\% when 
using the SRAM and the DDR with an 8 KB Cache and 18\% when using the DDR with a 256 B Cache. It also increases
the \ac{DPS}/MHz and \ac{DMIPS}/MHz by 35\% when using the SRAM or the DDR with an 8 KB Cache and by 22\% when 
using the DDR with a 256 B Cache.

This happens because the CPU outputs the \code{address} and \code{valid} signals of every memory access one 
cycle ahead of the regular memory interface, which accelerates memory writes and reads~\cite{bib:picorv32}. 
However, note that, on one hand, the external memory in RTL simulations is not an actual DDR but instead is an 
\ac{SRAM} with \ac{AXI} interface, which means that the actual \ac{DDR} timings in the \ac{FPGA} are worse than 
the \ac{AXI} \ac{SRAM}'s ones on the \ac{RTL} simulations. On the other hand, by using an external \ac{SRAM} 
and not \ac{DDR}, the effect of the \ac{LA} memory interface for IObSoC's external AXI4 Memory Interface is 
much better measured, as it does not account for further delays inherent to the \ac{DDR}.

The improvement caused by the \ac{LA} interface is greater on the SRAM and DDR + 8 KB Cache memory 
configurations than in the \ac{DDR} + 256 B Cache memory configuration, but is quite noticeable in both cases.

\section{FPGA Implementation Results}

IObSoC was also implemented on a Xilinx XCKU040-1FBVA676 \ac{FPGA} with -1 Speed Grade, hosted by
the KU040 board. All three IObSoC operating modes were successfully implemented on \ac{FPGA} running 
the Dhrystone benchmark with 100 runs (without the \code{-O3} flag in GCC), which completed with positive 
results, thus validating the SoC on \ac{FPGA}.

The \ac{SoC}'s clock frequency is 100 MHz and the \ac{LA} memory interface is disabled. The Cache on the 
\ac{DDR} operating mode has the same configuration as the 8 KB Cache used in the RTL simulation for comparison.

\ac{FPGA} resource utilization results are available in Table~\ref{table:fpga_resources}. As expected, the 
SoC operating modes that do not use the \ac{DDR} require a similar quantity of \ac{FPGA} resources -- \acp{LUT}, 
\acp{BRAM} and \acp{DSP} --, while the \ac{DDR} operating mode requires much more resources because it uses 
additional and larger components such as the Cache, the \ac{AXI} Interconnect and the \ac{MIG} core.

\begin{table}[!htb]
	\renewcommand{\arraystretch}{1.2} % more space between rows
	\caption{FPGA resource utilization for each IObSoC operating mode.}
	\centering
	\small
	\begin{tabular}{cc|ccc}
		\toprule
		DDR & Boot & LUTs & 36 KB BRAMs & DSPs \\
		\midrule
		No & No    & 1787  & 8     & 4\\
		No & Yes   & 1776  & 9     & 4\\
		Yes & Yes  & 13049 & 29.5  & 7\\
		\bottomrule
	\end{tabular}
	\label{table:fpga_resources}
\end{table}

\begin{table*}[!htb]
	\renewcommand{\arraystretch}{1.2} % more space between rows
	\caption{Dhrystone benchmark results on FPGA with 100 runs and a clock frequency of 100 MHz.}
	\centering
	\small
	\begin{tabular}{cc|cc|cccc}
		\toprule
		DDR & Boot & No. cycles & No. instructions & CPI    & DPS/MHz & DMIPS/MHz\\
		\midrule
		No  & No   & 267791     & 48719            & 5.496  & 373     & 0.212\\
		No  & Yes  & 267791     & 48719            & 5.496  & 373     & 0.212\\
		Yes & Yes  & 1023322    & 48719            & 21.004 & 97      & 0.055\\
		\bottomrule
	\end{tabular}
	\label{table:results_100_fpga}
\end{table*}

Dhrystone results in the \ac{FPGA} are presented in Table~\ref{table:results_100_fpga}, revealing 
that the SoC operating modes that run code from the internal \ac{SRAM} (i.e., the ones that do not 
use the \ac{DDR}) produce the exact same results for the Dhrystone benchmark on \ac{FPGA} and 
\ac{RTL} simulation. 

The \ac{DDR} operating mode, however, is less performant on \ac{FPGA} than in \ac{RTL} simulation because 
the external memory in the first case is a \ac{DDR}, while on the second case is an \ac{AXI} \ac{SRAM}, 
featuring faster memory access than a \ac{DDR}. For this reason, the execution time and the performance 
indicators turn out to be worse for the \ac{FPGA}.

Also, the Cache used on the \ac{FPGA} for the \ac{DDR} operating mode is a prior and less performant
version of the Cache used in the \ac{RTL} simulation tests, which does not yet work on \ac{FPGA} with 
IObSoC.

