\chapter{Conclusions}
\label{chapter:conclusions}

This document introduces IObSoC, a RISC-V \ac{SoC} developed at company IObundle that uses the
PicoRV32 CPU and other open-source components and can be configured to work in various operating 
modes. IObSoC is presented as a base \ac{SoC} of a development environment for RISC-V \acp{SoC}, 
which includes addition of new \ac{CPU} peripherals, software and hardware compilation, \ac{RTL} 
simulation and \ac{FPGA} implementation.

After considering several RISC-V \ac{CPU} options, the PicoRV32 processor architecture was chosen 
to build IObSoC, given its simplicity and validated examples. The SoC was then built alongside its
verification environment, so that it could be tested as its development advanced.

In order to validate IObSoC's operation, the SoC was tested with the Dhrystone 
benchmark~\cite{bib:dhrystone}, a widely used program for measuring general purpose processor 
performance. The tests were carried out with \ac{RTL} simulations of the several SoC's operating 
modes using the Icarus Verilog simulator. IObSoC was also implemented on \ac{FPGA} and successfully
executed the Dhrystone benchmark on all operating modes.

After this dissertation project's conclusion, IObSoC's development continued at IObundle. Some of its
most recent developments after this dissertation project are:

\begin{itemize}
	\item Creation of an \ac{ASIC} development flow, in order to produce \ac{ASIC} designs using IObSoC 
	(still underway);
	
	\item New \ac{CPU}-agnostic architecture and native interface of IObSoC, which is now a \ac{CPU}-agnostic 
	system, i.e., IObSoC no longer assumes a given \ac{CPU} core/architecture. The \ac{SoC} now has a \ac{CPU} 
	wrapper which contains one of several different \ac{CPU} cores. The \ac{CPU}'s ports are connected to the 
	\ac{CPU} wrapper, which then connects to the \ac{SoC}'s new native interface signals/buses, which now feature 
	separate buses for data and instructions (and respective address buses). The \ac{SoC} also now features two 
	separate caches for data and instructions. The supported \ac{CPU} cores are PicoRV32 and DarkRV~\cite{bib:darkrv}. 
	PicoRV32-specific features (such as the \ac{LA} memory interface) were discontinued;

	\item IObSoC's hardware definitions are now configured on a Makefile include file called \code{system.mk}, in the 
	root directory of IObSoC's repository. The \code{rtl/include/system.vh} file still exists, but now only it 
	interprets \code{system.mk}'s definitions to create Verilog macros that then are used in IObSoC's source files;
	
	\item IObSoC's operating modes have been further extended and customized by adding the new \code{USE\_SRAM} and 
	\code{RUN\_DDR} macros, making it possible to use the \ac{DDR} as auxiliary data storage while running code from
	the internal \ac{SRAM};
	
	\item The Soft Reset \& Boot Controller is no longer a peripheral and was moved to the	internal memory subsystem. 
	Soft resets are now performed by accessing an address of the memory subsystem. The \ac{SoC}'s only default peripheral 
	is now the \ac{UART}.
\end{itemize}

\section{Achievements}

The first achievement of this project was a new SoC and development environment using the innovative
RISC-V \ac{ISA}, which has become the new standard open and free \ac{ISA} and has great importance in
the open-source community. Besides, several large companies have already adopted RISC-V to design some 
of their systems.

The second achievement is the highly configurable SoC architecture, which features several operating modes
that allow it to be used in different contexts and applications, thus increasing the range of potential
clients for companies who adopt it. It also allows configuration of several parameters (such as memory sizes
and memory address map) in a single configuration file which is used by several tools.

The third achievement is the reduction of development time and effort of new \acp{SoC}. Further development
of IObSoC can be done systematically (as described in Chapter \ref{chapter:iobsoc_development}) and several
verification tools are already configured and ready-to-use via simple \code{make} commands in a terminal,
which accelerates development. This allows small start-up companies to to meet narrower deadlines and thus 
be able to compete in the digital circuit design market.

The fourth and final achievement is the reduction of resources and money spent on developing \acp{SoC}.
By using the open and free RISC-V \ac{ISA}, acquiring expensive licenses for CPUs in the form of \ac{IP} 
from large companies such as ARM is no longer a necessity. Instead, a free open-source CPU core -- such as
PicoRV32 -- can be used. By also using mostly open-source components and tools, the number of paid licenses 
is further reduced. This allows small start-up companies to develop their systems with a lower budget and
thus increase their competitiveness in the digital circuit design market.

\section{Future Work}

IObSoC can be further developed in several future work perspectives. The first one is to implement the 
\ac{SoC} on an \ac{ASIC}, which some customers may desire if the volume of \acp{SoC} needed is high enough 
to make the unitary price of each unit lower than that of implementing it on several \acp{FPGA}. Therefore, 
it is an advantage for a company to be able to produce their \acp{SoC} in \acp{ASIC}.

The second future work perspective is add support of new \ac{CPU} architectures for IObSoC (which is now easier 
thanks to the recent developments of the new \ac{CPU}-agnostic IObSoC architecture). This is an advantage because 
PicoRV32, although being small, has a relatively higher \ac{CPI}, thus being inadequate for high \ac{CPU} 
performance applications.

The third future work perspective is to create a new \ac{SoC} operating mode for running code from a flash memory 
unit. Flash is a cheap, reprogrammable and widely used non-volatile solid-state memory type. For these
reasons, many customers are interested in \ac{SoC} that can run a software stored in a flash memory. One way
to achieve this goal is to develop an \ac{SPI} flash controller that can be used as a peripheral in IObSoC.

The fourth and final future work perspective is to add \ac{OS} functionality to IObSoC. This is an important 
feature because it widens the range of supported applications by IObSoC, such as software programs that require 
the use of a file system. The RISC-V privileged \ac{ISA} provides privileged instructions required for \acp{OS}.