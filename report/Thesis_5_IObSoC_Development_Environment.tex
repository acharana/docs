\chapter{IObSoC Development Environment}
\label{chapter:iobsoc_development}

IObSoC is a base \ac{SoC} which can be further developed by adding new peripherals
which the application firmware then operates via software drivers to accomplish 
some goal. 

IObSoC's development environment is a collection of several software simulation and 
\ac{FPGA} implementation tools that are integrated using a Makefile tree. The Makefiles 
are invoked from a terminal on IObSoC's root repository using the \code{make <target>} 
command, which calls the tools specified in the Makefiles' scripts. Several Python and Bash 
scripts are used in the Makefiles for auxiliary purposes, such as (but not limited to):

\begin{itemize}
	\item converting IObSoC's \code{.vh} Verilog configuration file into a \code{.h} 
	C header file, used by the software sources; 
	\item converting \ac{GCC}'s output binary \code{.bin} file into a \code{.hex} file, which 
	the Verilog sources use to initialize memories with \code{\$readmemh} directives.
\end{itemize}

\section{Adding Peripherals}

\subsection{Creating the peripheral}
The peripheral must be designed with the Verilog \ac{HDL} and its ports 
must be compatible with IObSoC's native interface or with the 
AXI4-Lite interface~\cite{bib:axi_amba};

%-----------------------------------------------------------------------------------------

\subsection{Editing IObSoC's configuration file}

Next, IObSoC's configuration file, located in \code{rtl/include/system.vh}, must be 
edited according to the following steps:

\begin{itemize}
	\item Comment/uncomment the \code{USE\_BOOT} and \code{USE\_DDR} 
	macros, depending on the desired SoC operating mode;
	\item Configure \code{MAINRAM\_ADDR\_W} -- the \ac{SRAM}'s address width when used as Main 
	Memory --, depending on the size of the application (not needed for \ac{DDR} applications);
	\item Increment \code{N\_SLAVES} (i.e., the number of slaves/peripherals $N_S$);
	\item Increment \code{N\_SLAVES\_W} (i.e., $W_P$ given by Equation~\ref{eq:wp});
	\item Add a base address for the new peripheral;
	\item Uncomment/comment \code{USE\_LA\_IF} to use or not PicoRV32's \ac{LA} memory
	interface, respectively (simulation only).
\end{itemize}

The code example in Figure~\ref{fig:iob_timer_config} exemplifies how to configure 
IObundle's timer \ac{IP} -- IObTimer~\cite{bib:iobtimer} -- in IObSoC's configuration 
file. Edited lines contain a comment \code{// EDITED: <comment>}.

\lstset{language=verilog}
\lstset{basicstyle=\scriptsize}
\begin{figure}[!htb]
	\begin{minipage}{\linewidth}
		\begin{lstlisting}[frame=single]
//
// HARDWARE DEFINITIONS
//

//Comment/uncomment to choose IObSoC's operating mode
//`define USE_BOOT
//`define USE_DDR

//main memory address space (log2 of byte size)
`define MAINRAM_ADDR_W 15

//SLAVES
`define N_SLAVES 5 // EDITED: was 4 before

//bits reserved to identify slave
`define N_SLAVES_W 3 // ceil(log2(N_SLAVES)) // EDITED: was 2 before

//peripheral address prefixes
`define UART_BASE 0
`define SOFT_RESET_BASE 1
`define DDR_BASE 2
`define SRAM_BASE 3
`define TIMER_BASE 4 // EDITED: added this line

//use CPU lookahead interface
//`define USE_LA_IF

...
\end{lstlisting}
\end{minipage}
\caption{IObSoC's configuration file edit when adding a new IObTimer peripheral.}
\label{fig:iob_timer_config}
\end{figure}
\lstset{basicstyle=\normalsize}

%-----------------------------------------------------------------------------------------

\subsection{Instantiating the peripheral in IObSoC}

Next, IObSoC's top source file, located in \code{rtl/src/system.v}, must be edited.
The code excerpt in Figure~\ref{fig:iob_timer_inst} exemplifies how to instantiate 
IObTimer in IObSoC's top source file, inside its module definition
(between \code{module <name> (<io\_port\_list>)} and \code{endmodule}).

\lstset{language=verilog}
\lstset{basicstyle=\scriptsize}
\begin{figure}[!htb]
	\begin{minipage}{\linewidth}
		\begin{lstlisting}[frame=single]
`timescale 1 ns / 1 ps
`include "system.vh"
`include "int_mem.vh"

module system (
	...
	
	// clock, reset and native interface wires are already defined above
	
	time_counter #(.COUNTER WIDTH(32))
	timer (
		.rst     (reset_int),
		.clk     (clk),
		.addr    (m addr[2]),
		.data_in (m_wdata),
		.data_out(s_rdata[`TIMER_BASE]),
		.valid   (s_valid[`TIMER_BASE]),
		.ready   (s_ready[`TIMER_BASE])
	);
	
	...
endmodule
		\end{lstlisting}
	\end{minipage}
	\caption{IObTimer's instantiation in IObSoC's top level source file.}
	\label{fig:iob_timer_inst}
\end{figure}
\lstset{basicstyle=\normalsize}

If the peripheral has AXI4-Lite ports, a native-to-AXI adapter must be instantiated 
between it and the \ac{SoC}'s peripheral bus (which is not the case of IObTimer).
A generic code example of this is shown in Figure~\ref{fig:axi_per_inst}.

\lstset{language=verilog}
\lstset{basicstyle=\scriptsize}
\begin{figure}[!htb]
	\begin{minipage}{\linewidth}
		\begin{lstlisting}[frame=single]
`timescale 1 ns / 1 ps
`include "system.vh"
`include "int_mem.vh"

module system (
	...
	
	// clock, reset and native interface wires are already defined above
	
	// AXI peripheral interface wires definitions
	wire        axi_awvalid;
	wire        axi_awready;
	wire [31:0] axi_awaddr;
	wire        axi_wvalid;
	wire        axi_wready;
	wire [31:0] axi_wdata;
	wire [3:0]  axi_wstrb;
	wire        axi_bvalid;
	wire        axi_bready;
	wire        axi_arvalid;
	wire        axi_arready;
	wire [31:0] axi_araddr;
	wire        axi_rvalid;
	wire        axi_rready;
	wire [31:0] axi_rdata;
	
	axi_peripheral axi_p0 (
		.clk             (clk),
		.reset           (reset_int),
		
		// AXI interface
		.per_axi_awvalid (axi_awvalid),
		.per_axi_awready (axi_awready),
		.per_axi_awaddr  (axi_awaddr),
		.per_axi_wvalid  (axi_wvalid),
		.per_axi_wready  (axi_wready),
		.per_axi_wdata   (axi_wdata),
		.per_axi_wstrb   (axi_wstrb),
		.per_axi_bvalid  (axi_bvalid),
		.per_axi_bready  (axi_bready),
		.per_axi_arvalid (axi_arvalid),
		.per_axi_arready (axi_arready),
		.per_axi_araddr  (axi_araddr),
		.per_axi_rvalid  (axi_rvalid),
		.per_axi_rready  (axi_rready),
		.per_axi_rdata   (axi_rdata)
	);
	
	native_axi_adapter native2axi0 (
		.clk             (clk),
		.reset           (reset_int),
		
		// AXI interface
		.mem_axi_awvalid (axi_awvalid),
		.mem_axi_awready (axi_awready),
		.mem_axi_awaddr  (axi_awaddr),
		.mem_axi_wvalid  (axi_wvalid),
		.mem_axi_wready  (axi_wready),
		.mem_axi_wdata   (axi_wdata),
		.mem_axi_wstrb   (axi_wstrb),
		.mem_axi_bvalid  (axi_bvalid),
		.mem_axi_bready  (axi_bready),
		.mem_axi_arvalid (axi_arvalid),
		.mem_axi_arready (axi_arready),
		.mem_axi_araddr  (axi_araddr),
		.mem_axi_rvalid  (axi_rvalid),
		.mem_axi_rready  (axi_rready),
		.mem_axi_rdata   (axi_rdata),
		
		// Native interface
		.mem_addr        (m_addr),
		.mem_wdata       (m_wdata),
		.mem_wstrb       (m_wstrb),
		.mem_rdata       (s_rdata[`AXI_PER_BASE]),
		.mem_valid       (s_valid[`AXI_PER_BASE]),
		.mem_ready       (s_ready[`AXI_PER_BASE])
	);
	
	...
endmodule
		\end{lstlisting}
	\end{minipage}
	\caption{Instantiation of a generic AXI peripheral and respective native-to-AXI adapter in IObSoC's top level source file.}
	\label{fig:axi_per_inst}
\end{figure}
\lstset{basicstyle=\normalsize}

When a new peripheral is added to IObSoC, it may require new \ac{I/O} ports. In this case, 
the simulation's testbench and the \ac{FPGA}'s board-dependent wrapper will also require addition 
of new wire connections and/or components to make use of the new feature. The \ac{FPGA} constraint 
file (which specifies connections of the \ac{FPGA}'s physical pins) may also need to be updated to 
use additional resources on the\ac{FPGA} 

For example, if a new \ac{SPI} flash controller is added to IObSoC for the KU040 board implementation, 
the \ac{SoC} will require new \ac{I/O} serial ports for the \ac{SPI} interface. A new equivalent 
\ac{SRAM} with an \ac{SPI} interface must be instantiated in the testbench and connected to the 
\ac{SoC}'s \ac{SPI} interface. The board-dependent wrapper will need new wire connections (and possibly 
other interface) between the \ac{SoC} and the flash memory) between the \ac{SoC}'s new \ac{SPI} interface 
to the \ac{FPGA} pins that connect to the external flash memory chip on the KU040 board. 

%-----------------------------------------------------------------------------------------

\subsection{Firmware} Next, the peripheral's software drivers and the \ac{SoC}'s
firmware must be written:

\begin{itemize}
	\item Write one or more C source and header files with the software 
	drivers to operate the peripheral;
	
	\item Include the header file(s) in the firmware source file,
	located in \code{software/firmware/firmware.c} and develop the software
	application;
	
	\item If not using the \ac{DDR}, remember to edit the \code{MAINRAM\_ADDR\_W} 
	macro in IObSoC's configuration file so that the \ac{SRAM} is large enough to 
	store the firmware;
\end{itemize}

%-----------------------------------------------------------------------------------------

\subsection{Editing the Makefiles and the FPGA tools' scripts}

Next, the Makefiles and \ac{FPGA} tools' scripts in IObSoC's repository need to be 
updated with the new C and Verilog source and header files (or respective include
directories). The Makefiles and \ac{FPGA} tools' scripts to edit are:

\begin{itemize}
	\item \code{software/firmware/Makefile}: the firmware Makefile;
	\item \code{\$SIM\_DIR/Makefile}: the simulation Makefile;
	\item \code{\$FPGA\_DIR/Makefile}: the \ac{FPGA} Makefile;
	\item \code{\$FPGA\_DIR/<script(s)>}: the \ac{FPGA} script(s) (for example, Tcl script(s) 
	for Vivado);
\end{itemize}

\noindent where \code{\$SIM\_DIR} and \code{\$FPGA\_DIR} are the directories containing the 
Makefiles and scripts for each tool and must be defined in the top Makefile located in the 
root directory of IObSoC's repository, depending on the desired tools. The number of scripts 
to edit can vary between tools.

For example, to use the Icarus Verilog simulator, \code{SIM\_DIR = simulation/icarus} and 
thus the simulation Makefile is located in \code{simulation/icarus/Makefile}. 

Similarly, to use the Vivado tools for the KU040 board, \code{FPGA\_DIR = fpga/xilinx/AES-KU040-DB-G}. 
Therefore, the \ac{FPGA} Makefile is located in \code{fpga/xilinx/AES-KU040-DB-G/Makefile}. Vivado 
requires a Tcl script to generate the bitstream, which is \code{fpga/xilinx/AES-KU040-DB-G/synth\_system.tcl}. 

As an example, Figure~\ref{fig:firm_makefile} shows how to edit the firmware's Makefile to contemplate 
IObTimer's addition to the \ac{SoC} (assuming that IObTimer was added as a Git submodule of IObSoC). 
New/edited lines are preceded by a line containing a comment \code{\#\#\# <COMMENT>}.

\lstset{language=verilog}
\lstset{basicstyle=\scriptsize}
\begin{figure}[!htb]
	\begin{minipage}{\linewidth}
		\begin{lstlisting}[frame=single]
TOOLCHAIN_PREFIX := riscv32-unknown-elf-

PYTHON_DIR := ../python
SUBMODULES_DIR := ../../submodules
UART_DIR := $(SUBMODULES_DIR)/iob-uart/c-driver
CACHE_DIR := $(SUBMODULES_DIR)/iob-cache/c-driver

### SPECIFY TIMER DIRECTORY
TIMER_DIR := $(SUBMODULES_DIR)/iob-timer

### ADD TIMER DIRECTORY TO INCLUDE PATHS
INCLUDE = -I. -I$(UART_DIR) -I$(CACHE_DIR) -I$(TIMER_DIR)

DEFINE = -DUART_BAUD_RATE=$(BAUD) -DUART_CLK_FREQ=$(FREQ)

### ADD TIMER SOURCE FILE TO SOURCE LIST
SRC = firmware.c $(UART_DIR)/iob-uart.c $(CACHE_DIR)/iob-cache.c firmware.S \
      $(TIMER_DIR)/embedded/iob_timer.c)

### ADD TIMER HEADER FILE TO MAIN TARGET DEPENDENCY LIST
all: firmware.lds $(SRC) system.h $(UART_DIR)/iob-uart.h $(CACHE_DIR)/iob-cache.h $(TIMER_DIR)/iob_timer.h
$(TOOLCHAIN_PREFIX)gcc -Os -ffreestanding -nostdlib -march=rv32im -mabi=ilp32 -o firmware.elf $(DEFINE) \
      $(INCLUDE) $(SRC) --std=gnu99 -Wl,-Bstatic,-T,firmware.lds,-Map,firmware.map,--strip-debug -lgcc -lc
$(TOOLCHAIN_PREFIX)objcopy -O binary firmware.elf firmware.bin
$(eval MEM_SIZE=`../bash/get_binsize.sh firmware.bin`)
$(PYTHON_DIR)/makehex.py firmware.bin $(MEM_SIZE) > progmem.hex
$(eval MEM_SIZE=`$(PYTHON_DIR)/get_memsize.py MAINRAM_ADDR_W`)
$(PYTHON_DIR)/makehex.py firmware.bin $(MEM_SIZE) > firmware.hex

system.h: ../../rtl/include/system.vh
sed s/\`/\#/g ../../rtl/include/system.vh > system.h

clean:
@rm -rf firmware.bin firmware.elf firmware.map *.hex *.dat
@rm -rf uart_loader system.h
@rm -rf ../uart_loader

.PHONY: all clean
		\end{lstlisting}
\end{minipage}
\caption{Firmware Makefile's edits to contemplate IObTimer's software C drivers.}
\label{fig:firm_makefile}
\end{figure}
\lstset{basicstyle=\normalsize}
%-----------------------------------------------------------------------------------------

\section{Software}

The software executed in IObSoC is written with the C programming language and compiled 
with the GNU RISC-V \ac{GCC} cross-compiler~\cite{bib:riscvgnutools}. The software used 
inside the \ac{SoC} is the bootloader (in \code{software/bootloader}) and the firmware 
application (in \code{software/firmware}). 

RISC-V \ac{GCC} reads a set of C source (\code{.c}) files, C header (\code{.h}) files, assembly 
(\code{.S}) files and one linker script (\code{.lds}) file and uses them to generate an 
\ac{ELF} file (\code{.elf}). This file is then converted to a binary (\code{.bin}) file using GNU's
\code{objcopy} utility\footnote{
	The application firmware file received by IObSoC during the boot sequence is a \code{.bin} file.}
. The binary file in then converted to an \code{.hex} file\footnote{
	Which is an \ac{ASCII} equivalent form of the binary file.} 
that \ac{RTL} compilers accept for memory initialization purposes using Python and Bash scripts. 
If the files input in GCC have errors, the compiler will produce error and warning messages that 
the developer can use to debug the software.

The bootloader (running inside IObSoC) initializes the \ac{UART}, receives the application's 
binary file from it, initializes the Cache and finally soft resets the SoC while also writing 
a 0 to the boot register. Defining the \code{DEBUG} macro in the bootloader's C code activates 
a debug feature of sending back the application's binary file via \ac{UART}.

The bootloader feature is useful to accelerate the firmware application's debug because the 
firmware can easily be tested while running on an \ac{FPGA} due to the fact that it is possible 
to load a new firmware binary file to the \ac{SoC} via \ac{UART} without needing to recompile the 
bitstream, which is a process that requires a great amount of time.

The console program used by the computer to interact with IObSoC inside an \ac{FPGA} is 
also written in C, but compiled with the regular \ac{GCC} compiler on a Linux computer. It 
is stored in \code{software/ld-sw}. and is used to send the application's binary file to IObSoC
during its boot sequence and to receive print messages from the \ac{SoC}. Just like the bootloader,
it features a debug feature which receives the application's binary file back from the \ac{SoC} when
defining the \code{DEBUG} macro in its C code.

\section{RTL Simulation}

At the time of writing, the available \ac{RTL} simulators for IObSoC are Icarus 
Verilog~\cite{bib:icarus}, NCSim~\cite{bib:ncsim} and ModelSim~\cite{bib:modelsim}. 
To run an \ac{RTL} simulation, first edit \code{SIM\_DIR} in the top Makefile (for 
example, to use Icarus, define \code{SIM\_DIR = simulation/icarus}). Then \code{cd} 
to the repository's root and run \code{make sim}. This also compiles the bootloader 
and the firmware before the simulation starts.

The simulation's target is the Verilog testbench located in \code{rtl/testbench/system\_tb.v}.
The instantiated components inside of it are:

\begin{itemize}
	\item \textbf{IObSoC}, which is the \ac{UUT};
	\item An \textbf{UART}, which communicates with IObSoC's \ac{UART}. It is used to simulate
	the \ac{SoC} bootloader feature (i.e., load an application to the \ac{SoC}) and receive
	print messages;
	\item An \textbf{AXI Memory}, which connects to IObSoC's \ac{AXI} interface and is used as 
	the \ac{SoC}'s external memory to simulate IObSoC's \ac{DDR} operating mode. It can be
	pre-loaded with the application, unlike the real \ac{DDR} Memory;
\end{itemize}

Files specific to each \ac{RTL} simulator must be placed inside the respective simulator's 
directory, whose paths follow the template \code{simulation/<simulator>}.

\section{FPGA Emulation}

The available \ac{RTL} synthesis and \ac{FPGA} implementation tools for IObSoC are Intel's 
Quartus~\cite{bib:quartus} and Xilinx's Vivado~\cite{bib:vivado} and ISE~\cite{bib:ise}.
To generate a bitstream, first edit \code{FPGA\_DIR} in the top Makefile (for example, 
to use the KU040 board with Vivado, define \code{FPGA\_DIR = fpga/xilinx/AES-KU040-DB-G}). 
Then \code{cd} to the repository's root, \code{source} the FPGA tools' settings (if necessary) 
and run \code{make fpga}. If the \ac{FPGA} board is hosted by a remote machine, the \code{fpga} 
target also sends the bitstream and the firmware binary file to it via \ac{SCP}.

Then, on the \ac{FPGA} board's host machine -- which can be accessed via \ac{SSH} --, 
\code{cd} to the root directory of IObSoC's repository (which must also be cloned in it as 
well) and run \code{make ld-sw} to setup the console to interact with IObSoC. In a new 
terminal, source the \ac{FPGA} tools' settings if needed and run \code{make ld-hw} to load the
bitstream onto the \ac{FPGA}. When using the KU040 board, \code{make ld-hw} runs a Tcl script 
(\code{fpga/xilinx/AES-KU040-DB-G/ld-hw.tcl}) with Vivado that loads the bitstream onto the \ac{FPGA}.

The \ac{FPGA} implementation's target is the board-dependent wrapper Verilog source file, located
in \code{fpga/xilinx/AES-KU040-DB-G/verilog/top\_system.v} (already described in 
Chapter~\ref{chapter:iobsoc_architecture}). Files specific to each \ac{FPGA} (including \ac{RTL} 
files) must be placed inside the respective \ac{FPGA}'s directory, whose paths follow the template 
\code{fpga/<vendor>/<board>}.