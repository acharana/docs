\chapter{System on Chip Development}
\label{chapter:soc_design}

\section{Architecture}
\label{section:soc_architecture}

An \ac{SoC} is an electronic system completely incorporated in a single \ac{IC} (or chip).
\acp{SoC} can be implemented as an  \ac{ASIC} or on a programmable platform such as an 
\ac{FPGA}. An \ac{SoC} typically consists of one or more \acp{CPU} controlling other
hardware peripheral components.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Master-Slave Paradigm}
\label{section:master_slave_paradigm}

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.6\linewidth]{Figures/master_slave}
	\caption{Generic diagram illustrating a system with $N$ masters and $M$ slaves.}
	\label{fig:master_slave}
\end{figure}

Digital hardware systems usually follow the master/slave paradigm, in which there 
are components (masters) that control others (slaves). Such a system is depicted
in Figure \ref{fig:master_slave}. Slave components only operate by request of a 
master component. In \acp{SoC}, \acp{CPU} can be both masters or slaves of other 
\acp{CPU} or devices. Devices like memory units, accelerators or communication 
interfaces are normally slaves.

One example of the master-slave behaviour is when a master \ac{CPU} writes to a 
slave memory. The memory is requested by the \ac{CPU} to write a data word to a 
given address (provided by the \ac{CPU}). Memory reads are also an example of 
master-slave communication. When a master \ac{CPU} reads from a slave memory, is 
requests the memory to output (on the data bus) a data word stored on a specific 
memory address.

A device can be simultaneously a master of some devices and a slave of others. One
example of this is a memory controller (such as an \ac{SPI} flash controller), which 
is a master of a memory unit (such as a flash memory) but a slave of a \ac{CPU} or a 
microcontroller that reads code instructions from it.

\subsection{Valid-Ready Handshake Protocol}

A handshake protocol is needed to control data flow between masters and slaves in 
digital systems such as \acp{SoC}. A widely used standard for this purpose is the 
\textbf{valid-ready handshake protocol}, which is implemented with two 1-bit signals:

\begin{itemize}
	\item \textbf{valid}: a master output and slave input asserted by the master to 
	indicate that its data/request is valid;
	\item \textbf{ready}: a master input and slave output that is asserted by the 
	slaves when they are ready to start the data transaction.
\end{itemize}

\code{ready} can depend on \code{valid}, but the opposite must not happen, i.e., 
\code{valid} can not depend on \code{ready} so that combinational loops are avoided 
in the hardware. 

Any data transaction between a master and a slave has three different states:

\begin{enumerate}
	\item \textbf{Idle state}, which corresponds to no valid data, 
	whether the slave is ready or not (\code{valid} equal to 0);
	
	\item \textbf{Wait state}, which corresponds to valid data when 
	the slave is not yet ready (\code{valid} equal to 1 and \code{ready} 
	equal to 0). This state may not exist on implementations where 
	\code{ready} is asserted at all times;
	
	\item \textbf{Transfer state}, which corresponds to valid data and a 
	ready slave, which is when the data transaction occurs (\code{valid} 
	and \code{ready} both equal to 1).
\end{enumerate}

This means that in a data transaction, the master asserts \code{valid} 
when data is valid to send/receive. The slave then asserts \code{ready}
(if it has not yet done so) and the data transaction begins, after which 
both \code{valid} and \code{ready} signals are de-asserted once again.

When it comes to hardware implementation of this protocol, in practise 
\code{ready} is either always asserted or is asserted after (or at the 
same time) as the master asserts \code{valid}.

On one hand, an always asserted \code{ready} results in a simpler 
implementation and faster communication, as there are no delays between the 
assertion of \code{valid} and \code{ready} (i.e., no wait state), but 
assumes that the slave is always ready to operate when \code{valid} pulses 
from the master occur.

On the other hand, forcing \code{ready} to explicitly depend on \code{valid} 
is a safer implementation choice when it comes to guaranteeing correct data 
flow between slaves and masters because a (purposeful) delay between the 
assertion of both signals can be inserted and thus timing closure is better 
assured.

Both implementation choices are valid, but depend on the system's hardware 
architecture, capabilities and specifications.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{SoC Components}

The following are examples of components that make up an \ac{SoC}.

\subsubsection{\acp{CPU}}

\acp{CPU} control peripherals by running software programs that write to and read 
from them. Peripherals are added to each \ac{CPU} according to each application's 
specificities. \acp{CPU} are ideal for general purpose programs and there are several 
architectures available (free and commercial) for many kinds of applications and 
\acp{SoC}, from low-power \ac{IoT} controllers to high-performance computing.

\subsubsection{\acp{GPU}}

\acp{GPU} are highly modular cores used in \acp{SoC} for dedicated graphics computations 
and other tasks featuring high data-level parallelism. Modern \acp{GPU} are already equipped
with dedicated vector, tensor and ray-tracing cores. Target applications of \acp{GPU} 
include gaming, 3D computer-aided design, 3D animation rendering, cryptocurrency mining 
and physical simulations (on the academic research field).

Some applications can be greatly accelerated by a \ac{GPU}, provided that they have 
high data-level parallelism.

\subsubsection{Memory units}

Memory units are used in a \ac{SoC} to store program data or instructions. Several types
of memory with different characteristics exist:

\begin{itemize}
	\item \textbf{Volatile} or \textbf{non-volatile} memory, whether it loses its contents
	is powered off of not, respectively. Flash and \ac{ROM} non-volatile, while \ac{RAM} 
	and \ac{DDR} are volatile;

	\item Memories with different \textbf{access methods}, which dictate how the memories' 
	contents are accessed. There are four main memory access methods:
	
	\begin{itemize}
		\item \textbf{Sequential}, where memory words are accessed sequentially, in the
		order that they are stored in memory. \acp{ROM} and NAND Flash are examples of 
		sequential access memories;
		\item \textbf{Direct}, where data is access in the physical location where it is
		stored in the memory. Computer hard drive disks are an example of this access method;
		\item \textbf{Random}, where each memory word has an address which is used by the
		memory's address decoder to access it. This access method is faster than direct access
		and takes the same amount of time to access any word in the memory. \ac{RAM}, \ac{DDR}
		and NOR Flash are examples of random-access memory; 
		\item \textbf{Associative}, which accesses memory words not by an addressing scheme
		but by comparing an input memory tag with part of the stored data and accessing the
		matched contents. An example of associative memory is are \ac{CPU} caches.
	\end{itemize}

	\item \textbf{On-chip} or \textbf{off-chip} memory, whether it is located inside the 
	\ac{SoC}'s chip or not, respectively. \ac{SRAM} is used as on-chip memory and has less 
	latency than off-chip memory;

	\item \textbf{Dynamic} or \textbf{static} memory, whether its contents need to be 
	refreshed periodically or not, respectively (provided that the memory is being powered).
	\ac{DRAM} is dynamic, while \ac{SRAM} is static;

	\item \textbf{Solid-state} memory, which means that it is constituted by semiconductor 
	electronics only and not moving mechanical parts. \ac{RAM} and Flash are examples of 
	solid-state memories, while a computers' hard disk drives are not.
\end{itemize}

\subsubsection{Memory controllers}

A memory controller is a digital component that controls data flow in and out of a 
memory unit. Different kinds of memory require different control procedures. For example, 
\acp{DRAM} require periodic refreshing of their data, because each \ac{DRAM} memory cell 
consists of a pair of a small \ac{MOS} capacitor and a \ac{MOS} transistor, where the charge
of the capacitor dictates the logic value stored -- 0 or 1, whether it is charged or not.
Because the capacitors' discharge within a fraction of a second, \ac{DRAM} controllers need 
to periodically refresh the memory's contents so that the data is not lost on the capacitors'
discharge.

A simple \ac{SoC} architecture can centralize memory accesses on a \ac{CPU}, so that when a 
peripheral needs to access memory, it must do it through the \ac{CPU}. This can lead to high 
latency and slow throughput of memory accesses made by peripherals. To solve this issue, 
\acp{SoC} can use a feature called \ac{DMA}, which allows hardware peripherals and subsystems 
to access the memory independently of the \ac{CPU} by using a \ac{DMA} controller. However,
the use of \ac{DMA} causes cache coherency issues.

\subsubsection{On-chip module interconnection and communication interface}

A communication interface is a digital circuit that serves as a boundary between 
(at least) two hardware components that communicate by means of a communication protocol. 
A widely adopted industry standard communication interface is 
\ac{AXI}~\cite{bib:axi_amba}~\cite{bib:axi_xilinx}, developed by company ARM. It is free 
of royalties and its specification is freely available~\cite{bib:axi_amba}. Hardware design 
companies across the world can use \ac{AXI} in their hardware systems.

However, custom native interfaces can be and are adopted is many \acp{SoC} as well, leaving 
\ac{AXI} for compatibility with third-party designs, such as a peripheral or power management
unit (which can then be connected with native-to-\ac{AXI} adapters to the \ac{SoC}'s native
interface).

\subsubsection{Serial protocol interfaces/controllers}

A simple way for a \ac{CPU} to communicate with other external electronic systems is through 
serial interfaces because they are easy to implement. A serial interface is an hardware 
infrastructure that implements a serial communication protocol, which is a kind of 
communication where information is exchanged via one or more sequences of bits (i.e., voltage 
pulses) -- between digital systems. Examples of serial interfaces are \ac{UART}, \ac{SPI}, 
\ac{I2C}, Ethernet, \ac{USB}, \ac{HDMI} and \ac{PCIe}.

Depending on the system's intended specifications and application, several serial interfaces
with different characteristics -- such as throughput, latency, area, power consumption, etc. --
are available to choose. For example, a \ac{UART} can be used to send/receive char bytes to/from 
a computer and an \ac{SPI} module can be used to access an \ac{SPI} flash memory unit, which is 
one of the cheapest kind of off-chip non-volatile memory nowadays.

\subsubsection{Co-processors}

Co-processors or other dedicated peripherals can be used to supplement some functions of the 
\acp{CPU}, such as dedicated floating-point arithmetic cores, vector cores and others.

\subsubsection{\acp{CGRA}}

\acp{CGRA} -- such as IObundle's Versat~\cite{bib:versat} -- are particularly useful for 
implementing dedicated hardware accelerators because they feature several internal functional 
units -- such as adders, multipliers, accumulators, etc. -- whose connections can be 
reconfigured via software for each application. Therefore, they can be used as dedicated 
modules while providing flexibility for reconfiguration and can be used in high performance 
applications that also require low power consumption.

\subsubsection{Other dedicated modules}

Some \acp{SoC} may require modules that implement specific functions for more dedicated 
applications, such as audio and video encoders or decoders.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{SoC Intermodule Communication}

\subsubsection{Traditional bus infrastructure}

Traditionally, the various components that make an \ac{SoC} communicate with each
other via a hardware bus infrastructure, in which information is exchanged via
parallel electrical wires (called buses). An \ac{SoC} is said to be a $n$-bit \ac{SoC}
when its buses are $N$-bit wide (where $N$ is typically a power of 2, such as 16, 32 or 
64).

This kind of intermodule communication is not scalable for \ac{IoT} because when the 
number of cores and/or components in an \ac{SoC} rises above a certain level, the bus
infrastructure needed to ensure communication between all components grows to a
point where silicon area and power consumption are too high to meet the tight 
specifications of \ac{IoT} applications.

\subsubsection{Network on Chip}

A solution to this problem is implementing network structures based on network
protocols such as \ac{TCP} and the Internet Protocol and routing algorithms such 
as Dijkstra's. This approach is called \ac{NoC} and greatly reduces power consumption
and silicon area occupied by wires connecting \ac{SoC} modules, while also improving
communication throughput and latency \cite{bib:noc}. 

Although this approach is much more interesting than traditional bus communication 
infrastructures for \ac{IoT} applications, it also requires network knowledge such as 
network communication protocols and routing algorithms, as well as how these can be 
implemented in hardware (routers).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{RISC-V CPU Architectures}

Several RISC-V CPU architectures were taken into account to build IObSoC:

\begin{itemize}
	
	\item \textbf{Rocket Chip}~\cite{bib:rocketchip}, a synthesizable \ac{SoC} 
	\ac{RTL} generator made by the same team that introduced RISC-V. It has yielded 
	functional silicon prototypes that boot the Linux \ac{OS} and can use the 
	following \ac{CPU} generators: 
	
	\begin{itemize}
		\item \textbf{Rocket}, an in-order scalar core generator that can implement
		the RV32G\footnote{Which is an 
			abbreviated designation for RV32IMFAD, the RV32I base \ac{ISA} 
			with extensions M, F, A and D.}
		and the RV64G\footnote{Which is an 
			abbreviated designation for RV64IMFAD, the RV64I base \ac{ISA} 
			with extensions M, F, A and D.}
		\acp{ISA};
		\item \textbf{BOOM}, an out-of-order, superscalar RV64G core generator;
	\end{itemize}
	
	\item \textbf{Taiga}~\cite{bib:taiga} is a high-performance RISC-V
	softcore written in SystemVerilog, developed mainly for use in \acp{FPGA}
	and for supporting multicore configurations using an \ac{OS};
	
	\item \textbf{PULPino}~\cite{bib:pulpino} is an open source single-core
	microcontroller system that can be configured to use either one of two 
	different in-order, single-issue \ac{CPU} architectures for ultra low power 
	designs\footnote{
		Ultra low power in 	electronic circuits means that the transistors are 
		operated with the minimal power supply possible. As such, the transistors 
		are operated at sub-threshold regions, with ultra low threshold voltages.}
	~\cite{bib:ultralowpower}:

	\begin{itemize}
		\item \textbf{RI5CY}~\cite{bib:riscy}, which supports the RV32IFMC \ac{ISA}, 
		has 4 pipeline stages and an \ac{IPC} and implements a subset of version 1.9 
		of RISC-V's privileged \ac{ISA}~\cite{bib:riscvprivileged19};	
		\item \textbf{zero-riscy}~\cite{bib:zeroriscy}, a \ac{CPU} architecture 
		derived from RI5CY. It features 2 pipeline stages and implements the 
		RV32IMC \ac{ISA}, but can also be configured as an RV32E core. Just like 
		RI5CY, it implements a subset of version 1.9 of the privileged RISC-V 
		\ac{ISA};
	\end{itemize}

	PULPino provides a set of peripherals such as \ac{I2S}, \ac{I2C}, \ac{SPI} and 
	\ac{UART} for communication with external systems. PULPino, RI5CY and 
	zero-riscy are written with the SystemVerilog \ac{HDL};
	
	\item \textbf{PULP}~\cite{bib:pulp} is an advanced microcontroller architecture 
	which consists of a more complete and complex system than PULPino, featuring new 
	peripherals, a new memory subsystem and numerous other improvements. Similarly 
	to PULPino, it can use either a RI5CY or zero-riscy \ac{CPU} as main core;
	
	\item \textbf{PicoRV32}~\cite{bib:picorv32} is a size-optimized RISC-V
	processor core that implements the RV32IMC \ac{ISA}. Although
	being small, it features a high maximum clock frequency (250-450 MHz
	on 7-Series Xilinx \acp{FPGA}). It is also possible 
	to configure it as a RV32E core. It also supports other optional useful 
	features such as a built-in interruption controller, a co-processor 
	interface and a memory \ac{LA} interface;
	
\end{itemize}

Rocket Chip has a great amount of features but it has been found too hard to
manipulate in the past at IObundle, so it was put aside. Taiga was also put
aside as it is solely optimized for \acp{FPGA}, thus not being suited for
\acp{ASIC}, which is a future goal for IObSoC. PULPino proved hard to detach 
from its environment in the past at IObundle and its features go beyond the 
scope of this project. PULP, although a major improvement over PULPino, is not 
yet implemented on \ac{FPGA}, so it is not a viable option yet.

PicoRV32 is easy to deal with and well documented, with several validated 
examples available, such as Raven~\cite{bib:raven}, an \ac{ASIC} implementation 
of PicoSoC (which is PicoRV32's example \ac{SoC}~\cite{bib:picorv32}). PicoRV32 
has been chosen for this project given its simplicity, validated examples and 
also because its repository is complete and well organized, with helpful 
documentation and useful open-source files that can be used.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{PicoSoC: An Example SoC Architecture}
\label{section:picosoc}

\begin{figure}[!h]
	\centering
	\includegraphics[width=\linewidth]{Figures/picosoc}
	\caption{PicoSoC's module diagram (taken from \cite{bib:picorv32}).}
	\label{fig:picosoc}
\end{figure}

PicoSoC is the example SoC provided in PicoRV32's repository \cite{bib:picorv32}.
It runs code directly from an \ac{SPI} flash memory chip and can be used as a 
simple controller in \ac{FPGA} or \ac{ASIC} designs.

PicoSoC is a memory mapped-system, which means that the \ac{CPU} writes to/reads 
from peripherals by specifying an address which the \ac{SoC} then decodes in order 
to know which peripheral is being selected. Each peripheral has its own memory-mapped 
region, which are defined in Table \ref{table:picosoc_memory_map}.

\begin{table}[!htb]
	\renewcommand{\arraystretch}{1.2} % more space between rows
	\caption{PicoSoC's memory map (taken from \cite{bib:picorv32}).}
	\centering
	\small
	\begin{tabular}{l|l}
		\toprule
		Address Range & Description\\		
		\midrule
		0x00000000 .. 0x00FFFFFF & Internal SRAM\\
		0x01000000 .. 0x01FFFFFF & External Serial Flash\\
		0x02000000 .. 0x02000003 & SPI Flash Controller Config Register\\
		0x02000004 .. 0x02000007 & UART Clock Divider Register\\
		0x02000008 .. 0x0200000B & UART Send/Recv Data Register\\
		0x03000000 .. 0xFFFFFFFF & Memory mapped user peripherals\\
		\bottomrule
	\end{tabular}
	\label{table:picosoc_memory_map}
\end{table}

PicoSoC has four components: a PicoRV32 \ac{CPU}, a \ac{UART}, an \ac{XIP} 
\ac{SPI} Flash Controller and an internal scratchpad\footnote{
	A \textbf{scratchpad} memory is a small, high speed memory unit
	that is the next closest memory to the \ac{CPU} other than its register 
	file used for temporary data storage.}
\ac{SRAM}.

\subsubsection{PicoRV32 CPU}

The \ac{CPU} is the main component and single master of PicoSoC. It controls all 
peripherals (slaves) by running software programs stored in memory, which use 
C drivers (i.e., software drivers written in the C programming language) specifically 
made to control each peripheral. These drivers are sets of C functions which mainly 
consist in writing to or reading from memory-mapped registers in the peripherals and 
other derived procedures.	

The CPU is a PicoRV32 core~\cite{bib:picorv32}, an open source processor that 
implements the RV32IMC \ac{ISA}. PicoRV32 is a size-optimized \ac{CPU} with high 
maximum frequency and that can also be configured to use the base RV32E \ac{ISA}. 
Some of its key features are:

\begin{itemize}
	\item Small size. When deployed on a 7-Series Xilinx \ac{FPGA}, it uses
	between 750 and 2000 \acp{LUT};
	\item Maximum frequency between 250 MHz and 450 MHz on 7-Series Xilinx
	\acp{FPGA};
	\item \ac{CPI} approximately equal to 4, but depends on the mix of
	instructions in the code;
	\item Selectable native memory interface or AXI4-Lite master interface 
	and native-to-AXI interface adapters written in Verilog for multi-core
	\acp{SoC};
	\item Optional interrupt handler using a simple custom \ac{ISA};
	\item Optional Co-Processor Interface, which can be used to implement
	non-branching instructions in external co-processors, such as the RISC-V
	M extension;
	\item Option to choose single-port or dual-port register file
	implementation, which can be useful to either reduce the core's
	size or to increase its performance;
\end{itemize}

\subsubsection{UART}

An \ac{UART} is an hardware module that implements the RS232 serial protocol, 
which describes an asynchronous\footnote{
	A communication is \textbf{asynchronous} when the transmitted and received bits 
	are not synchronized with a clock signal and \textbf{synchronous} when it is.} 
serial communication. It can be simplex\footnote{
	A communication between two devices is called \textbf{simplex} when one is a 
	transmitter and the other is a receiver and these roles never swap. When both 
	devices can act as transmitter and receiver, it is said that the communication 
	is \textbf{duplex}. A duplex communication can be further categorized as 
	\textbf{full-duplex} or \textbf{half-duplex}, whether transmission and reception 
	can occur simultaneously or not, respectively.}, 
full-duplex or half-duplex, depending on the hardware implementation of the module. 

PicoSoC's \ac{UART} is mainly used for printing purposes, i.e., it sends char bytes 
to a computer that can be visualized in a terminal and features full-duplex communication.

\subsubsection{XIP SPI Flash Controller}

An \ac{SPI} is a synchronous, full-duplex, single-master multi-slave serial interface 
that is usually used in \acp{SoC} to communicate with an \ac{SPI} flash memory unit 
which stores program data or code. Typically, it has larger bandwidth than a \ac{UART} 
because it can support data bursts and also can have more data channels, while the 
\ac{UART} has only one. \ac{SPI} flash memory is one of the cheapest kind of off-chip 
non-volatile memory available nowadays, which makes the use of an \ac{SPI} Flash 
Controller even more pertinent.

PicoSoC has an \ac{XIP} \ac{SPI} Flash Controller, which allows program code execution 
directly from the flash memory instead of copying the program from the flash to a \ac{RAM}.
It also features several operation modes, which are (regular) \ac{SPI}, dual \ac{SPI} and 
\ac{QSPI}, which use 1-bit, 2-bit and 4-bit data buses, respectively.

\subsubsection{Scratchpad SRAM}

PicoSoC has an internal scratchpad \ac{SRAM} that can be used to store auxiliary data that 
needs to be accessed fast by the \ac{CPU}. The \ac{SRAM} has greater access speed than the 
\ac{SPI} flash memory chip\footnote{
	\ac{SRAM} is typically used in \ac{CPU} caches.}. 
In PicoSoC's top Verilog source file, the \ac{SRAM}'s size is set to 1 KB, which accounts 
for a total of 256 32-bit words, but its size can be increased by editing this file, 
if necessary.

This scratchpad \ac{SRAM} is not used for program code execution in PicoSoC. Instead, 
program code is stored in the \ac{SPI} flash memory and is executed directly from it. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Tools}
\label{section:soc_dev_env}

\acp{SoC} are systems which made of both hardware and software components.
In order to successfully design an \ac{SoC} for an \ac{ASIC} or \ac{FPGA}, 
several software tools must be used to build the software and hardware components. 
Most of these tools are commercial although effective open source tools are 
becoming more and more popular, which is an advantage for small companies.

\subsection{Software Toolchain}

In order to produce software for a RISC-V \ac{CPU}, both the GNU~\cite{bib:riscvgnutools} 
and LLVM toolchains~\cite{bib:llvm} have added support for the RISC-V architectures. 
These toolchains offer software tools such as:

\begin{itemize}
	\item \textbf{Compilers}, which translate programming language to machine 
	level language, creating executable programs;
	\item \textbf{Assemblers}, which convert assembly language to machine level 
	language, creating executable programs;
	\item \textbf{Linkers}, which combines several object files generated by 
	compilers/assemblers into a single executable file;
	\item \textbf{Debuggers}, which are used test, evaluate and debug programs;
	\item \textbf{Profilers}, which analyse a program and profile information 
	such as memory space and time complexity, frequency and duration of function 
	calls, etc. These profiles are then interpreted by the developer to optimize 
	the program.
\end{itemize}

\subsection{RTL Simulators}

\ac{RTL} simulation is an essential step for verifying the correctness of the
design. NCSim~\cite{bib:ncsim} from Cadence and ModelSim~\cite{bib:modelsim}
from Mentor are well known commercial \ac{RTL} simulators which are supported 
in this work. Free and open-source \ac{RTL} simulators such as Icarus 
Verilog~\cite{bib:icarus} and Verilator~\cite{bib:verilator}.

An \ac{RTL} simulator reads sets of \ac{HDL} (such as Verilog or VHDL) source files to
compile them and perform a simulation which can generate a \ac{VCD} file. Developers 
then can feed the generated \ac{VCD} file to a \ac{VCD} waveform viewer such as GTKWave 
(which is free and open-source) and debug the \ac{HDL} sources until simulation provides 
positive results.

\subsection{FPGA Implementation Tools}

After successfully simulating a system's RTL code, the \ac{FPGA} emulation phase can 
begin. An \ac{FPGA} is a reconfigurable \ac{IC} that emulates hardware systems. The 
largest \ac{FPGA} manufacturers are Xilinx and Intel. Both provide proprietary software 
suites for \ac{RTL} synthesis and \ac{FPGA} implementation tools for their own \acp{FPGA}. 
The Xilinx tool is called Vivado~\cite{bib:vivado} and the Intel tool is called 
Quartus~\cite{bib:quartus}. These suites require paid licenses that are provided under 
different commercial options. For example, one type of license is granted when acquiring 
one of their \ac{FPGA} development boards. 

To use an \ac{FPGA} to emulate an hardware system, the system's \ac{HDL} files and constraints 
files\footnote{
	A \textbf{constraint file} is a file in which the connections between the \ac{FPGA}'s 
	pins and the ports of the system it emulates are defined.} 
are input on the \ac{FPGA}'s manufacturer's software tools to translate the \ac{HDL} code into 
a bitstream that is loaded onto the \ac{FPGA}, configuring it in a manner such that the 
\ac{HDL}-described system is emulated inside of it.

To generate a bitstream, the synthesis and implementation tools perform the following steps:

\begin{itemize}
	\item \textbf{\ac{RTL} synthesis}, which is the process of converting the system's \ac{RTL} 
	(described in \ac{HDL} files) into an equivalent generic gate-level description;

	\item \textbf{\ac{FPGA} implementation}, which takes as input the system's generic 
	gate-level description output by the synthesis step. It consists of 3 steps: 
	\begin{itemize}
		\item \textbf{Placement}, which determines where the system's logic blocks are placed 
		according to the \ac{FPGA}'s available resources;
		\item \textbf{Routing}, which determines how placed components must be connected/wired 
		together;
		\item \textbf{Bitstream generation}, which generates the bitstream from the output 
		of the place and route phases.
	\end{itemize}
\end{itemize}

\subsection{Toolchain Integration}

In order to integrate all the necessary tools to design \ac{SoC} in a single environment, 
a build automation tool\footnote{
	A \textbf{build automation tool} is a software tool that automates the various processes 
	associated with creating a software build, such as source compilation, simulation and 
	other tests.} 
must be used. Make and FuseSoC are examples of build automation tools that can be used for
integration of tools for \ac{SoC} development. PicoRV32's repository \cite{bib:picorv32} 
supports both Make and FuseSoC development flows, which are executed in a terminal.

\subsubsection{Make and Makefiles}

One of the most widely used build automation tools is \textbf{Make}, which automates the building
process of executable programs from files called \textbf{Makefiles} in Unix and Unix-based 
operating systems (such as Linux). A Makefile is a script file that contains shell-like commands 
organized in targets that can depend on each other and thus automate build tasks in simple or 
complex schemes.

Makefile targets are called/invoked from a terminal. When a target is called, Make automatically 
detects if that target and the targets which it depends on need to be reran by analysing the 
various file dependencies and when they were most recently edited. If no files changed, then a 
target may not need to run again, as its output would be the same. By avoiding unnecessary 
rerunning of complex programs take a lot of time (such as \ac{FPGA} synthesis/implementation), 
development is accelerated.

It is possible for a Makefile to invoke targets in separate Makefiles in different local 
directories and even run and automate Make commands in remote machines via Secure Shell 
(SSH\footnote{
	SSH is a cryptographic network protocol that allows secure operation in unsecure 
	networks.}
). Thus, a repository can have a top level Makefile that calls targets on Makefiles
located deeper inside the repository's directory structure.

Makefiles can be used to manipulate environment variables, such as feeding them into software 
tools invoked in them and exporting them to separate environments/Makefiles, which allows
tool customization in an automated manner.

\subsubsection{FuseSoC}
\label{section:fusesoc}

FuseSoC \cite{bib:fusesoc} is a build system for digital hardware systems that also
works as a package manager for reusable hardware blocks and as a toolchain integration
mechanism.

FuseSoC provides libraries of \ac{HDL} files with hardware components for building 
\acp{SoC} such as \ac{UART}, \ac{I2C}, \ac{FIFO} modules and many others, which are 
hosted in a GitHub repository in \cite{bib:fusesoc_cores}. FuseSoC also allows to use 
other components by creating user libraries.

FuseSoC's operation revolves around \textbf{core description files}, which
are terminated with a \code{.core} extension. A core description file specifies
which \ac{HDL} sources are necessary to compile, simulate and/or synthesize a given 
core. It is possible to define core dependencies (for example, a core may require 
a version higher or equal to 1.1 of a certain component).

FuseSoC allows integration of several simulation and synthesis tools in core 
description files, including tool-specific options to several tools (such as 
simulation timescale). This way, it is possible to customize simulation of cores
separately. It is yet possible to define file generators that generate necessary
files for the core to be compiled or simulated (for example, an \ac{SoC} that needs 
an \code{.hex} file generated by \ac{GCC} from a set of \code{.c} and \code{.h} files).

FuseSoC's core description files also allow targets (similar to Makefile targets) that 
can be executed in a terminal. For example, each simulation tool can have its own
target. Therefore, FuseSoC can serve as an interesting alternative to a Makefile system 
to integrate the various SoC verification tools.