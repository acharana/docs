%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%     File: Thesis_Implementation.tex                                  %
%     Tex Master: Thesis.tex                                           %
%                                                                      %
%     Author: Andre C. Marta                                           %
%     Last modified :  2 Jul 2015                                      %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Instruction Set Architectures}
\label{chapter:isa}

An \ac{ISA} is a set of instructions and their specifications which is used as an 
abstract model of a computing system, such as a \ac{CPU} or a microcontroller (which 
are implementations of the \ac{ISA}). Therefore, an \ac{ISA} can be thought of as the 
boundary between the hardware implementation of a \ac{CPU} and its software capabilities,
as it specifies hardware details necessary to implement a given set of software 
instructions.

\section{RISC vs CISC}
\label{section:riscvscisc}

There are two types of \acp{ISA}: \ac{RISC} and \ac{CISC}. The desktop
PC market has been dominated by Intel's x86 \ac{ISA}, a 16-bit \ac{CISC} \ac{ISA} 
that was first introduced in 1978. In 1985, x86 was expanded to 32-bit and in 
2003 to 64-bit. However, in the embedded systems market, companies like ARM 
gained the upper hand with \ac{RISC} processor architectures in the 
21\textsuperscript{st} century.

On the one hand, \ac{RISC} provides a small set of simple instructions that allow 
the machines they are implemented in to have a reduced \ac{CPI} value. On the other hand, 
as opposed to \ac{RISC}, \ac{CISC}\footnote{The term \ac{CISC} is usually used to refer 
	to any non-\ac{RISC} computer system. For example, a microcontroller that does not
	separate memory loads/stores from arithmetic operations can be labelled as
	\ac{CISC}.} provides a large number of instructions where each one is a sequence 
of several lower level operations (or micro-operations), For example, a \ac{CISC} 
instruction can be a memory load of one or more operands followed by some arithmetic 
operation and a subsequent memory store of the result.

\subsection{Performance Comparison Between RISC and CISC}
\label{subsections:riscciscperformance}

\ac{RISC} and \ac{CISC} are two different approaches to tackle the performance 
problem in \acp{CPU}. The performance of a benchmark program can be expressed as 
the inverse of the time $t$ taken for the program's execution to complete, which 
in turn is usually given by the Equation~\ref{eq:performance} (known as the
Performance Equation), where:

\begin{itemize}
	\item $N_I$ is the number of instructions of the benchmark program;
	\item $CPI$ is the average number of clock cycles per instruction;
	\item $T_{CLK} = \frac{1}{f_{CLK}}$ is the clock period (i.e. the 
	duration of one clock cycle);
	\item $f_{CLK} = \frac{1}{T_{CLK}}$ is clock frequency;
\end{itemize}

% Performance equation
\begin{equation}
\label{eq:performance}
t = N_I \cdot CPI \cdot T_{CLK} = \frac{N_I \cdot CPI}{f_{CLK}},
\end{equation}

In order to increase performance, \ac{CISC} tries to reduce the number of 
instructions $N_I$ while sacrificing the \ac{CPI}. \ac{RISC} does the opposite,
i.e., it reduces the \ac{CPI} while sacrificing $N_I$.

% CISC performance
\ac{CISC} architectures often have higher \ac{CPI} values because the instructions 
are usually multi-step sequences of simpler operations which take one clock cycle
each (or a few more), resulting in instructions that take several clock cycles
to execute. Because the instructions are built this way, they can be much more
specific and custom. For this reason, \ac{CISC} \acp{ISA} have a huge number
of instructions, with many different sets of similar ones that differ only in
very specific details. This allows for an \ac{ISA} to have a very complete set of
instructions, which results in extremely small and efficient assembly codes and
therefore a small number of instructions $N_I$ in programs.

% RISC performance
\ac{RISC} architectures are characterized by a small set simple instructions that execute 
in a minimal number of clock cycles, targeting low \ac{CPI} values, which means that they
need to be used much more often in a given program. A consequence of this feature is 
the increase of the programs' code size, as more instructions are needed to perform the 
same operations done by \ac{CISC} instructions. This explains why $N_I$ in \ac{RISC} 
\acp{ISA} is higher than in \ac{CISC} \acp{ISA}.

% RISC and CISC debate is irrelevant nowadays
\subsubsection{RISC vs CISC performance conclusions}
As referred in~\cite{bib:ciscriscirrelevant}, the performance differences between 
\ac{RISC} and \ac{CISC} were more important in the 1980's than in modern
times, because the key constraints back then were chip area and the
processor design complexity. Nowadays, the primary constraints are low energy
and power consumption, where using \ac{RISC} or \ac{CISC} seems irrelevant; ARM's 
\ac{RISC} cores have penetrated the high-performance servers market (previously 
dominated by Intel's x86 \ac{CISC} cores) while Intel's x86 \ac{CISC} cores have 
penetrated the mobile low power devices market (previously dominated by ARM's 
\ac{RISC} cores).

Also in \cite{bib:crisc} it is stated that the \ac{RISC} vs \ac{CISC} debate is of 
no more interest nowadays, as it mentions that today's \ac{CISC} \acp{CPU} 
(i.e., x86 \acp{CPU}) are not true \ac{CISC} cores anymore, but instead are \ac{CRISC}
cores, which means that they still execute the x86 \ac{CISC} instruction
set, but their internal implementation is based in \ac{RISC} principles. This means
that the internal behaviour of both types of \acp{ISA} may not be so different, which
may justify why this choice does not affect performance.

% Brainiac microarchitecture designs influence performance greatly
According to~\cite{bib:ciscriscirrelevant}, the differences in performance
between the ARM Cortex-A8 and Cortex-A9 and Intel Atom and Sandybridge i7
microprocessors are due to the microarchitecture features of the various cores
such as out-of-order execution, instruction-level parallelism (i.e., superscalar 
processors), cache hierarchies and policies, speculative execution (i.e. branch 
predictors), data-level parallelism (i.e. vector processors) and register 
renaming.

Microarchitecture features seem to have a significant impact on performance. In
particular, the x86 cores manage to maintain high performance mainly due to
the highly accurate branch predictor and large caches and not because of the
\ac{ISA} type. So, the conclusion is that the \ac{RISC} vs \ac{CISC} performance 
debate is irrelevant nowadays.

\subsection{Advantages and Disadvantages of RISC and CISC}
\label{subsections:riscciscadvantages}

Although performance is not an issue in the \ac{RISC} vs \ac{CISC} debate, 
there are other factors that differentiate \ac{RISC} and \ac{CISC} cores, which
are summarized in Table \ref{table:riscvscisc}.

\begin{table}[!htb]
	\renewcommand{\arraystretch}{1.2} % more space between rows
	\caption{Advantages and disadvantages of RISC and CISC.}
	\centering
	\small
	\begin{tabular}{l|ll}
		\toprule
		Feature                                   & RISC                     & CISC \\
		\midrule
		Size of instruction set                   & Small                    & Large\\
		Complexity of instructions                & Simple                   & Complex\\
		Clock cycles per instruction              & High                     & Low\\
		Instructions per program (programs' size) & Large                    & Small\\
		Compatibility with legacy systems         & No                       & Yes (x86)\\
		Free and open-source ISAs                 & Yes                      & No\\
		Cost of licensing a core                  & Depends, but can be free & High\\
		\bottomrule
	\end{tabular}
	\label{table:riscvscisc}
\end{table}

As explained before, \ac{RISC} \acp{ISA} lead to instruction sets with lower size and 
simpler instructions, larger program sizes and higher \ac{CPI} values than \ac{CISC} 
\acp{ISA}.

There are no open-source \ac{CISC} \ac{CPU} architectures and the only \ac{CISC} 
\acp{CPU} used nowadays use Intel's proprietary x86 \ac{ISA}, which prevailed along 
the years mainly because of reasons of compatibility with older legacy systems. It
requires a paid license either from Intel -- to design an x86 core -- or from a vendor 
to use an existing x86 core. There are, however, several free, open-source and commercial 
\ac{RISC} \ac{CPU} architectures available, which is the decisive factor for small 
companies and start-ups.

\section{The RISC-V ISA}
\label{section:riscvisa}

The main free (to use) \ac{ISA} nowadays is RISC-V, a \ac{RISC} \ac{ISA} and surrounding 
software ecosystem that allows standard and custom extensions of its base \ac{ISA}. Open 
source hardware designs and software toolchains -- such as the GNU~\cite{bib:riscvgnutools} 
and LLVM~\cite{bib:llvm} RISC-V toolchains -- are now vastly available for download by any 
interested individual or organization.

\subsection{Brief History}

The RISC-V project started in 2010 at the University of California, Berkeley. 
Since then, many contributors such as volunteers and industry workers have joined 
the project, building a large RISC-V community. Start-up SiFive was created in 
2015 to encourage RISC-V dissemination in the semiconductor and electronic design 
industries. On November 29, 2016, SiFive released the first \ac{IC} that implements 
the RISC-V \ac{ISA} and in October 2017 they released the first \ac{SoC} that 
supports fully featured \acp{OS} like Linux, which validated RISC-V's potential to 
become an industry standard in the future.

In 2018, the RISC-V \ac{ISA} continues to gain importance in the semiconductor \ac{IP}
industry. Large companies like NVidia and Western Digital Corp. \textit{"have 
	decided to use RISC-V in their own internally developed 
	silicon. Western Digital's chief technology officer has said that in 2019 or
	2020, the company will unveil a new RISC-V processor for the more than 1 
	billion cores the storage firm ships each year. Likewise, Nvidia is using 
	RISC-V for a governing microcontroller that it places on the board to manage
	its massively multicore graphics processors"} \cite{bib:opinion}.

RISC-V continues to grow in the hardware design industry, as more and more companies 
choose it to develop their systems free of licensing costs. Several free open source 
\ac{CPU} architectures that implement the RISC-V \ac{ISA} are already available online
on Git repositories hosted by platforms such as GitHub and Bitbucket.

\subsection{Unprivileged ISA}
\label{section:unprivilegedisa}

RISC-V supports four base \acp{ISA} and several standard extensions, whose names and 
status can be consulted in Table~\ref{table:isa}. It is also possible to build custom 
extensions for the RISC-V \ac{ISA} like, for example, \ac{GPU} based instruction sets.

At the time of writing of this document, the latest version of the RISC-V unprivileged
architecture is described in the RISC-V unprivileged architecture manual~\cite{bib:riscvmanual}. 
The RISC-V base modules and extensions and their respective status can be consulted on 
Table~\ref{table:isa}, where:

\begin{itemize}
	\item modules marked \textbf{Ratified} have been ratified in the latest version;
	\item modules marked \textit{Frozen} are not expected to change significantly before 
	being put up for ratification;
	\item modules marked \textit{Draft} are expected to change before ratification.
\end{itemize} 

% Tabela: https://github.com/riscv/riscv-isa-manual/blob/master/src/preface.tex
\begin{table}[!htb]
	\renewcommand{\arraystretch}{1.2} % more space between rows
	\caption{The RISC-V unprivileged \ac{ISA} status (taken from~\cite{bib:riscvmanual}, Preface, page i).}
	\centering
	\small
	\begin{tabular}{clc}
		\hline
		Base	       & Version & Status\\
		\hline	
		RVWMO          & 2.0     & \bf Ratified   \\
		\bf RV32I      & \bf 2.1 & \bf Ratified \\
		\bf RV64I      & \bf 2.1 & \bf Ratified \\
		\em RV32E      & \em 1.9 & \em Draft \\
		\em RV128I     & \em 1.7 & \em Draft \\
		\hline
		Extension      & Version & Status \\
		\hline
		\bf M          & \bf 2.0 & \bf Ratified \\
		\bf A          & \bf 2.1 & \bf Ratified \\
		\bf F          & \bf 2.2 & \bf Ratified \\
		\bf D          & \bf 2.2 & \bf Ratified \\
		\bf Q          & \bf 2.2 & \bf Ratified \\
		\bf C          & \bf 2.0 & \bf Ratified \\
		\em Counters   & \em 2.0 & \em Draft \\
		\em L          & \em 0.0 & \em Draft \\
		\em B          & \em 0.0 & \em Draft \\
		\em J          & \em 0.0 & \em Draft \\
		\em T          & \em 0.0 & \em Draft \\
		\em P          & \em 0.2 & \em Draft \\
		\em V          & \em 0.7 & \em Draft \\
		\bf Zicsr      & \bf 2.0 & \bf Ratified \\
		\bf Zifencei   & \bf 2.0 & \bf Ratified \\
		\em Zam        & \em 0.1 & \em Draft \\
		\em Ztso       & \em 0.1 & \em Frozen \\
		\bottomrule
	\end{tabular}
	\label{table:isa}
\end{table}

The base modules of the RISC-V \ac{ISA} are:

\begin{itemize}
	\item \textbf{RV32I}, which can be seen as the "true" RISC-V integer base \ac{ISA}, 
	with 32-bit instructions;
	\item \textbf{RV64I}, which is built upon RV64I, expanding its instruction set 
	to 64 bits; 
	\item \textbf{RV128I}, result of an extrapolation of RV32I and RV64I to build
	a 128-bit \ac{ISA};
	\item \textbf{RV32E}, a reduced version of RV32I useful for embedded systems;
	\item \textbf{\ac{RVWMO}}, the RISC-V default memory 
	consistency model, which is the set of rules that specifies the values that can 
	be returned by loads of memory. As opposed to strong memory models, weak memory models 
	have the advantages of greater flexibility for hardware implementations, performance, 
	power and scalability at the expense of a more complex programming model\footnote{A 
		programming model describes all the information needed to program a\ac{CPU} or 
		microcontroller, such as the internal registers accessible by programs and their 
		roles}.
\end{itemize} 

The ratified extensions of the RISC-V \ac{ISA} are: 

\begin{itemize}
	\item \textbf{M}, for integer multiplication and division instructions; 
	\item \textbf{A}, which provides atomic instructions, i.e., instruction that read and 
	write in the same memory position in order to prevent other CPU core or I/O device to
	access it before the instruction is completed;
	\item \textbf{F}, \textbf{D}, and \textbf{Q}, for floating point arithmetic instructions 
	in single (32-bit), double (64-bit) and quadruple (128-bit) precision, respectively;
	\item \textbf{C}, for compressed instructions, which are 16-bit or half-sized versions of 
	the base \acp{ISA}'s instructions;
	\item \textbf{Zicsr}, which adds a set of \ac{CSR} instructions to operate \acp{CSR} 
	associated with harts (hardware threads);
	\item \textbf{Zifencei}, which adds the FENCE.I instruction for explicit synchronization 
	between writes to instruction memory and instruction fetches on the same hart.
\end{itemize}

The other extensions contemplated in Table~\ref{table:isa} are not yet ratified.

The most important modules of the RISC-V \ac{ISA} for this project are briefly described next.

\subsubsection{RV32I}

RV32I is the fundamental base integer RISC-V \ac{ISA}. It contains a total of 40 instructions,
which are listed in Table~\ref{table:rv32i} alongside their respective encodings. It uses 32 
general purpose 32-bit registers, named x0 to x31, where x0 has all bits hardwired to 0. A 
separate register is used as program counter, storing the memory address of the current 
instruction.

An important detail in Table~\ref{table:rv32i} is that all RV32I instructions have opcodes 
where the two least significant are equal to 1. As explained later on, these two bits are used 
to select either regular or the optional compressed instructions of the RISC-V C extension.

\input{rv32i_instr_table}

This instruction set contains integer arithmetic and logic operations such as addition,
subtraction, bit-wise OR, AND and XOR, logic and arithmetic shifts, etc both for register 
operand pairs and register operand and immediate value pairs. It also contains instructions 
to perform memory loads and stores, fundamental in any \ac{ISA}. Control transfer 
instructions for unconditional jumps and conditional branch are also included.

A memory ordering instruction called FENCE is included to order \ac{I/O} and memory accesses
as seen by other harts or external devices. Do not mistake this instruction with the 
FENCE.I instruction of the Zicfencei extension.

RV32I includes two system instructions for environment call and breakpoints: ECALL and EBREAK.
Both of these instructions cause a trap to the execution environment. ECALL is used to make
service requests to the execution environment, while EBREAK returns control the the debugging
environment.

For further details on RV32I, consult~\cite{bib:riscvmanual}, Chapter 2 (page 13).
\subsubsection{RV32E}

RV32E is a reduced version of RV32I useful for embedded systems. It uses only 16 registers (half 
than RV32I), which saves around 25\% of area on a small \ac{CPU} cores~\cite{bib:riscvmanual}. 

RV32E uses the same instruction set and encoding as RV32I, but disables floating point instructions, 
(i.e., it is incompatible with the F, D and Q extensions) which can be substituted by software routines.

The use of the RISC-V C extension alongside RV32E is recommend to further optimize memory size 
(reducing the total area of the system) and increase power efficiency, which are key constraints
on embedded systems. The advantages of the RISC-V C extension are described later on.

For further details on RV32E, consult~\cite{bib:riscvmanual}, Chapter 4 (page 33).

\subsubsection{RV32M}

RV32M is the name used to identify the RV32I base \ac{ISA} equipped with the M extension, 
for integer multiplication and division instructions. These instructions are separated from the
base \acp{ISA} so that RISC-V can be used in low-end systems that do not require or heavily use
these operations.

The RV32M instruction set is contemplated in Table~\ref{table:rv32m}. Several instructions for
integer multiplication, division and division remainder are included, which can operate on signed
and unsigned operands. There are multiplication instructions that store the most significant bits
of the 64-bit multiplication result for several signed/unsigned operand configurations.

For further details on the RISC-V M extension, consult~\cite{bib:riscvmanual}, Chapter 7 (page 43).
\input{rv32m_instr_table}

\subsubsection{RV32F}
RV32F is the name used to identify the RV32I base \ac{ISA} equipped with the F extension,
which adds the single-precision floating-point instructions listed in Table~\ref{table:rv32f}.
RV32F adds a floating-point register file consisting of 32 registers named f0 to f31, each 
32-bit wide. It also adds a floating-point \ac{CSR} named \code{fcsr}, which has status 
information of the floating-point unit. For this reason, RV32F requires the Zicsr extension
to operate the \code{fcsr} \ac{CSR}.

\input{rv32f_instr_table}

RV32F features several floating-point operations such as addition and subtraction, multiplication
and division, multiply-add and multiply-subtract, square root, minimum and maximum number, etc. 
It also features dedicated floating-point memory store and load instructions. 

For further details on the RISC-V F extension, consult~\cite{bib:riscvmanual}, Chapter 11 (page 63).

\subsubsection{RVC}

RVC is a generic name used to identify any of the base RISC-V base \acp{ISA} (RV32I, RV64I or 
RV128I) alongside the RISC-V C extension for compressed instructions. The RVC opcode map is
shown in Table~\ref{table:rvc}.

RISC-V's 32-bit instructions result in simple implementations but also in large code sizes, 
which is a typical characteristic of \ac{RISC} architectures. To mitigate this issue, the C 
extension is provided to allow compressed instructions, which are 16-bit versions of the regular 
instructions. This RISC-V extension succeeds in reducing code size by 25\% to 30\%, thus reducing 
the number of instruction cache misses by 20\% to 25\% and therefore improving energy efficiency 
and performance (with a similar performance gain as doubling the size of the instruction 
cache)~\cite{bib:compressed}.

% Table taken from https://github.com/riscv/riscv-isa-manual/blob/master/src/rvc-opcode-map.tex
\vspace{0.1in}
\definecolor{gray}{RGB}{180,180,180}
\begin{table}[htbp]
	\begin{center}
			\caption{RVC opcode map (taken from~\cite{bib:riscvmanual}, Chapter 16, page 112).}
		
		{\footnotesize
			\setlength{\tabcolsep}{4pt}
			\begin{tabular}{|r|c|c|c|c|c|c|c|c|l}
				\cline{1-9}
				inst[15:13] & \multirow{2}{*}{000}& \multirow{2}{*}{001}& \multirow{2}{*}{010}& \multirow{2}{*}{011}& \multirow{2}{*}{100}& \multirow{2}{*}{101}& \multirow{2}{*}{110}& \multirow{2}{*}{111}\\ \cline{1-1}
				inst[1:0] & & & & & & & & \\ \cline{1-9}
				\multirow{3}{*}{00} & \multirow{3}{*}{ADDI4SPN} & FLD   & \multirow{3}{*}{LW}   & FLW                           & \multirow{3}{*}{\em Reserved}  & FSD                & \multirow{3}{*}{SW}   & FSW                   & RV32  \\
				&                           & FLD   &                       & LD                            &                                & FSD                &                       & SD                    & RV64  \\
				&                           & LQ    &                       & LD                            &                                & SQ                 &                       & SD                    & RV128 \\ \hline
				\multirow{3}{*}{01} & \multirow{3}{*}{ADDI}     & JAL   & \multirow{3}{*}{LI}   & \multirow{3}{*}{LUI/ADDI16SP} & \multirow{3}{*}{MISC-ALU}      & \multirow{3}{*}{J} & \multirow{3}{*}{BEQZ} & \multirow{3}{*}{BNEZ} & RV32  \\
				&                           & ADDIW &                       &                               &                                &                    &                       &                       & RV64  \\
				&                           & ADDIW &                       &                               &                                &                    &                       &                       & RV128 \\ \hline
				\multirow{3}{*}{10} & \multirow{3}{*}{SLLI}     & FLDSP & \multirow{3}{*}{LWSP} & FLWSP                         & \multirow{3}{*}{J[AL]R/MV/ADD} & FSDSP              & \multirow{3}{*}{SWSP} & FSWSP                 & RV32  \\
				&                           & FLDSP &                       & LDSP                          &                                & FSDSP              &                       & SDSP                  & RV64  \\
				&                           & LQSP  &                       & LDSP                          &                                & SQSP               &                       & SDSP                  & RV128 \\ \cline{1-9}
				\cellcolor{gray} 11  & \multicolumn{8}{c|}{\cellcolor{gray} $>$16b} \\ \cline{1-9}
			\end{tabular}
		\label{table:rvc}
		}
	\end{center}
\end{table}

RV32I's instructions were built to use, in reality, only 30 bits instead of the total 32 bits. 
Two bits of the opcode (\code{instr[1:0]} on Table~\ref{table:rvc}) are used to select 1 out 
of 4 (optional) instruction subsets, each one corresponding to a row in Table~\ref{table:rvc}. 
The 3 subsets corresponding to combinations of \code{instr[1:0]} with at least one bit equal to 
0 are the optional compressed instruction subsets. The subset with \code{instr[1:0]} equal to 
binary 11 corresponds to the regular-sized instructions of the base \ac{ISA}. The three bits 
\code{instr[15:13]} (columns on Table~\ref{table:rvc}) are then used to identify the instruction 
inside the selected subset.

Compressed instructions are 16-bit wide and expand to 32-bit instructions on all base \acp{ISA} 
(not just on RV32I) by means of a compressed instruction alias scheme. This means that compressed 
instructions are aliases of the larger base \ac{ISA}'s instructions. This alias scheme is also 
used in other compressed \ac{RISC} \acp{ISA} like ARM's Thumb and the MIPS16.

However, RVC has an advantage over the previous two, because it allows mixing both regular and
compressed instructions on the same program without having to worry about operation modes like 
in Thumb or MIPS16, where only one of the two types of instructions can be used in a program.

RVC instructions can be directly expanded in the instruction decode stage, which accounts for 
less and simpler hardware. The instruction fields are scrambled across the instruction word 
so that most its bits are in the same positions on both compressed and regular instructions, 
which simplifies and reduces the hardware needed to decode either type of instruction. This 
makes a RISC-V processor core much more energy-efficient.

In conclusion, RVC is particularly useful for embedded systems and \ac{SoC} designs, because 
it provides smaller code sizes and more energy efficiency~\cite{bib:compressed}. Using it
alongside RV32E is also quite useful in these kinds of systems.

For further details on the RISC-V C extension, consult~\cite{bib:riscvmanual}, Chapter 26 (page 97).

\subsection{Privileged ISA}
There is also a privileged level instruction set in the RISC-V \ac{ISA}~\cite{bib:riscvprivileged}, 
which details privileged instructions and other functionalities required for \ac{OS} support and 
attaching external devices. A RISC-V core that uses the privileged architecture also requires the
\ac{CSR} instructions provided by the Zicsr extensions of the unprivileged \ac{ISA}.

The privileged level of the RISC-V \ac{ISA} is out of the scope of this project and, therefore, 
is not further detailed in this document.